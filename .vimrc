set nocompatible
filetype off
set autoindent
syntax on
set number
set visualbell
set encoding=utf-8
set expandtab
set tabstop=2 
" Required because of our new status bar
set noshowmode
set laststatus=2

" Split the Window by Default
set splitbelow
set termwinsize=20x0

" Open terminal below by default
autocmd VimEnter * bel terminal

" Stop files being opened in NERD_tree buffer
nnoremap <expr> <Leader>s (expand('%') =~ 'NERD_tree' ? "\<c-w>\<c-w>" : '').":Ack!\<Space>"

" Plugin Manager Documentation
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" init the plugins
call plug#begin('~/.vim/plugged')

" When the plugin has installed
" Reload .vimrc and :PlugInstall to install plugins.

" Actual List of Plugins

Plug 'dracula/vim', { 'as': 'dracula' } " Testing the Theme
Plug 'preservim/nerdtree'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'pangloss/vim-javascript'    
Plug 'leafgarland/typescript-vim' 
Plug 'maxmellon/vim-jsx-pretty' 
Plug 'itchyny/lightline.vim'
Plug 'prettier/vim-prettier', {
  \ 'do': 'npm install',
  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html'] }

" Initialize plugin system
call plug#end()

" Theme (after PlugInstall) and after the Plugin system ends
colorscheme dracula

" Fix for colourschemes in Kitty
let &t_ut=''

" Mapping for fzf
nnoremap <C-p> :<C-u>Files<CR> 
silent! nmap <C-P> :GFiles<CR>
" fzf in a window
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6, 'highlight': 'Todo' } }

" Set options for nerdtree
autocmd vimenter * NERDTree
map <C-n> :NERDTreeToggle<CR>
